class agrolog {

  $user = "ernesto"
  $basepath = "/home/${user}/workspace"
  $path = "${basepath}/agrolog"
  $gitRepo = "ssh://git@bitbucket.org/ernespn/agrolog.git"
  #$gitRepo = "https://ernespn@bitbucket.org/ernespn/agrolog.git"
  #$sshkey = "/home/${user}/.ssh/id_rsa_agrolog" 
  $folders = [$path]

  #folders = ["/home/${user}/workspace", $path]
  #user {$user:
  #  ensure => present,
  #}

  file {$path:
    ensure => directory,
    owner => $user
  }
  
  file {"/home/ernesto/.ssh":
    mode => 700,
  }  

  #package { "ssh-askpass":
  #  ensure => present,
  #}
   
  #file {$sshkey :
  #  owner => $user,
  #  mode => 600,
  #  source => "puppet:///modules/agrolog/id_rsa_agrolog",
  #  require => Package["ssh-askpass"]
  #}
 
  rbenv::install { "rbenv-install":
    user => $user,
    home => $path,
  }

  $rubyversion = "jruby-1.7.8" 

  rbenv::compile { $rubyversion :
    user => $user,
    home => $path,
    global => true,
  }

  rbenv::gem { "rails":
    user => $user,
    ruby => $rubyversion ,
  }

  class { 'postgresql::server': }

  postgresql::server::db { "agrolog":
    user     => 'postgres',
    password => postgresql_password('postgres', 'password'),
  }

  vcsrepo { $path:
    ensure => present,
    provider => git,
    source => $gitRepo,
    revision => 'master',
    user => $user,
    #require => File[$sshkey],
  }

}
