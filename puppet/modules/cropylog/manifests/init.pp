class cropylog{
  $user = "ernesto"
  $cropylog_virtualenvs = "/home/${user}/workspace/virtualenvs/cropylog"
  $folders = [$cropylog_virtualenvs]
  $pathRepo = "${cropylog_virtualenvs}/source"
  $gitRepo = "git://github.com/ernespn/cropylog"
  include user

  file {$folders:
    ensure => directory,
    owner => $user
  }

  python::virtualenv {$cropylog_virtualenvs:
    owner => $user,
    systempkgs   => true,
    notify => Exec['activate_virtualenv_cropylog'],
  }
  ->
  exec { 'activate_virtualenv_cropylog':
    command => "bash -c 'source ${cropylog_virtualenvs}/bin/activate'",
    path => "/usr/local/bin:/usr/bin:/bin:/usr/local/sbin:/usr/sbin:/sbin",
  }
  
  python::pip {'install_django':
    pkgname => 'django',
    virtualenv => "/home/${user}/workspace/virtualenvs/cropylog",
    owner => $user,
    require => Exec['activate_virtualenv_cropylog']
  }
  
  python::pip {'install_fabric_cropylog':
    pkgname => 'Fabric',
    virtualenv => "/home/${user}/workspace/virtualenvs/cropylog",
    owner => $user,
    require => Exec['activate_virtualenv_cropylog']
  }

  vcsrepo { $pathRepo:
    ensure => present,
    provider => git,
    source => $gitRepo,
    revision => 'master',
    user => $user,
    require => Exec['activate_virtualenv_cropylog'],
  }
}
