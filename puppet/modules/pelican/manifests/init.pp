class pelican{
  $user = "ernesto"
  $pelican_virtualenvs = "/home/${user}/workspace/virtualenvs/pelican"
  $folders = [$pelican_virtualenvs]
  $pathRepo = "${pelican_virtualenvs}/source"
  $gitRepo = "git://github.com/ernespn/blog"
  include user

  file {$folders:
    ensure => directory,
    owner => $user
  }

  python::virtualenv {$pelican_virtualenvs:
    owner => $user,
    systempkgs   => true,
    notify => Exec['activate_virtualenv_pelican'],
  }
  ->
  exec { 'activate_virtualenv_pelican':
    command => "bash -c 'source ${pelican_virtualenvs}/bin/activate'",
    path => "/usr/local/bin:/usr/bin:/bin:/usr/local/sbin:/usr/sbin:/sbin",
  }
  
  python::pip {'install_pelican':
    pkgname => 'pelican',
    virtualenv => "/home/${user}/workspace/virtualenvs/pelican",
    owner => $user,
    require => Exec['activate_virtualenv_pelican']
  }
  
  python::pip {'install_markdown':
    pkgname => 'Markdown',
    virtualenv => "/home/${user}/workspace/virtualenvs/pelican",
    owner => $user,
    require => Exec['activate_virtualenv_pelican']
  }
   
  python::pip {'install_fabric':
    pkgname => 'Fabric',
    virtualenv => "/home/${user}/workspace/virtualenvs/pelican",
    owner => $user,
    require => Exec['activate_virtualenv_pelican']
  }

  python::pip {'install_ghp_import':
    pkgname => 'ghp-import',
    virtualenv => "/home/${user}/workspace/virtualenvs/pelican",
    owner => $user,
    require => Exec['activate_virtualenv_pelican']
  }

  vcsrepo { $pathRepo:
    ensure => present,
    provider => git,
    source => $gitRepo,
    revision => 'master',
    user => $user,
    require => Exec['activate_virtualenv_pelican'],
  }
}
