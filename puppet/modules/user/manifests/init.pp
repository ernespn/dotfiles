class user {

 $user = "ernesto"
 user {$user:
    ensure => present,
  }
  $folders = ["/home/${user}/workspace", "/home/${user}/workspace/virtualenvs"]
  file {$folders:
    ensure => directory,
    owner => $user
  }

}
