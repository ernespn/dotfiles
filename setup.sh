# bash

#run puppet
puppet/runpuppet.sh

#vim
cp ~/.vim ~/.vim.bak
cp ~/.vimrc ~/.vimrc.bak
ln -s ~/dotfiles/vim ~/.vim
ln -s ~/dotfiles/vim/vimrc ~/.vimrc

#bash
cp ~/.bashrc ~/.bashrc.bak
cp ~/.bash_aliases ~/.bash_aliases.bak
cp ~/.bash_profile ~/.bash_profile.bak
cp ~/.dircolors ~/.dircolors.bak
cp ~/.profile ~/.profile.bak 
ln -s ~/dotfiles/bash/bashrc ~/.bashrc
ln -s ~/dotfiles/bash/bash_aliases ~/.bash_aliases
ln -s ~/dotfiles/bash/bash_profile ~/.bash_profile
ln -s ~/dotfiles/bash/dircolors ~/.dircolors
ln -s ~/dotfiles/bash/profile ~/.profile

#git
cp ~/.gitconfig ~/.gitconfig.bak
cp ~/.git-prompt.sh ~/.git-prompt.sh.bak
ln -s ~/dotfiles/git/gitconfig ~/.gitconfig
chmod +x ~/dotfiles/git/git-prompt.sh
ln -s ~/dotfiles/git/git-prompt.sh ~/.git-prompt.sh
